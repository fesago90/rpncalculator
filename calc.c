/*
 * Felipe Salazar
 * March 3, 2012
 *
 * Simple stack-based 'RPN' calculator as per http://programmingpraxis.com/2009/02/19/rpn-calculator/
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MAX_INPUT_LENGTH 255
#define EXEC_EXIT   -1
#define EXEC_NORMAL 0

typedef enum { 
    ExecQuit = 0,
    ExecNormal = 1,
    ExecInvalid = 2
} ExecResult;

double *stack = NULL;
int stack_elems = 0;

double pop_stack() {
    if (stack_elems > 0) {
        stack_elems--;
        double num = stack[stack_elems];
        stack = realloc(stack, sizeof(double)*stack_elems);
        return num;
    } else {
        puts("No operands in the stack\n");
        assert(0);
    }
}

void empty_stack() {
    while (stack_elems > 0)
        pop_stack();
}

void push_stack(double num) {
    stack_elems++;
    stack = realloc(stack,sizeof(double)*stack_elems); 
    stack[stack_elems-1] = num;
}

void print_stack() {
    int i;
    putchar('[');
    for (i = 0; i < stack_elems - 1; i++)
        printf("%f ", stack[i]);
    printf("%f]\n", stack[i]);
}

char* fetchExpression() {
    static char expression[MAX_INPUT_LENGTH + 1];
    printf(">> ");
    fgets(expression, MAX_INPUT_LENGTH, stdin);

    char *nl = NULL;
    if (nl = strchr(expression, '\n'))
        *nl = '\0';

    return expression;
}

int charIsOperator(char c) {
    return (c == '+' || c == '-' || c == '*' || c == '/');
}

double execOperator(double lnum, double rnum, char op) {
    switch (op) {
        case '+':
            return lnum + rnum;
        case '-':
            return lnum - rnum;
        case '*':
            return lnum * rnum;
        case '/':
            return lnum / rnum;
        default:
            printf("Logic incosistency. Invalid operator: %c\n", op);
            assert(0);
    }
}

ExecResult parseChunk(char const *chunk) {
    char op;
    double num, opL, opR;
    if (sscanf(chunk, "%lf", &num) == 1) {
        push_stack(num);
    } else if (sscanf(chunk, "%c", &op) == 1 && charIsOperator(op)) {
        if (stack_elems < 2) {
            printf("Cannot execute '%c', only %d operands in memory\n", op, stack_elems);
            return ExecInvalid;
        }
        opR = pop_stack(); 
        opL = pop_stack();
        push_stack(execOperator(opL, opR, op));
    } else {
        return ExecInvalid;
    }

    return ExecNormal;
}

ExecResult execExpression(char *expression) {

    if (strcmp(expression, "q") == 0)
        return ExecQuit;
    else if (strcmp(expression, "c") == 0) {
        empty_stack();
        return ExecNormal;
    }

    char *chunk = strtok(expression, " ");
    while (chunk != NULL) {
        if (parseChunk(chunk) == ExecInvalid) {
            puts("Invalid expression\n");
            return ExecInvalid;
        }
        chunk = strtok(NULL, " "); 
    }

    return ExecNormal;
}

int main(int argc, char *args[]) {
    char *expression = NULL;
    printf("Enter RPN expression and press enter\n");

    while (1) {
        expression = fetchExpression();

        if (feof(stdin)) return 0;

        switch (execExpression(expression)) {
            case ExecQuit:
                return 0;
            
            case ExecInvalid: 
                break;
            
            case ExecNormal:
                if (stack_elems == 1)
                    printf("%f\n", stack[stack_elems-1]); 
                else if (stack_elems > 1)
                    print_stack();
                break;
        }
    }

    return 0;
}
